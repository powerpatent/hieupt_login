export const CHANGE_EMAIL = 'CHANGE EMAIL'
export const CHANGE_PASSWORD = 'CHANGE PASSWORD'

export function actionChangeEmail(email){
    return {
        type: CHANGE_EMAIL,
        email
    }
}

export function actionChangePassword(password){
    return {
        type: CHANGE_PASSWORD,
        password
    }
}