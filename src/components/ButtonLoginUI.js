import React, {PropTypes} from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import '../css/LoginForm.css'

const ButtonLoginUI = ({clickSubmit}) =>(
    <div className='buttonLogin'>
        <RaisedButton label='LOGIN' onClick={clickSubmit} />
    </div>
)

ButtonLoginUI.PropTypes = {
    clickSubmit: PropTypes.func.isRequired
}

export default ButtonLoginUI