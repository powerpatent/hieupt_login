import React from 'react'
import Field from '../containers/Field'
import ButtonLogin from '../containers/ButtonLogin'
import '../css/LoginForm.css'


const LoginForm = () => (
    <div className='formLogin'>
        <div className='labelLogin'>PowerPatent</div>
        <Field />
        <ButtonLogin />
    </div>
)

export default LoginForm