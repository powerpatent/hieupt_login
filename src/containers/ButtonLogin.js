import {connect} from 'react-redux'
import ButtonLoginUI from '../components/ButtonLoginUI'
import request from 'request'

const mapState = (state) => {
    return {
        clickSubmit: () => {
            // do something
            console.log('Request {email: ', state.email, ', password: ', state.password)
            request.post({url:'http://54.253.12.41:8100/api/users/login',
                form: {email:state.email, password:state.password}},
                function(err,httpResponse,body){
                 /* do something ... */
                    if(!err && httpResponse.statusCode === 200){
                        console.log('Login is OK: ', body)
                    }

                })
        }
    }
}

const ButtonLogin = connect(
    mapState
)(ButtonLoginUI)
export default ButtonLogin