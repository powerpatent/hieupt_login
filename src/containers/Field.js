import React from 'react'
import {connect} from 'react-redux'
import {actionChangeEmail, actionChangePassword} from '../actions/actionLogin'
import '../css/LoginForm.css'

import TextField from 'material-ui/TextField';

let Field = ({dispatch}) => (
    <div className='fieldLogin'>
        <div >
            <TextField
            hintText="Email"
            floatingLabelText="Email"
            onChange={(e)=>{
                e.preventDefault()
                dispatch(actionChangeEmail(e.target.value))
            }}/>
        </div>
        <div>
            <TextField
            hintText="Password"
            floatingLabelText="Password"
            type="password"
            onChange={(e)=>{
                e.preventDefault()
                dispatch(actionChangePassword(e.target.value))
            }}
            />
        </div>
    </div>
)

Field = connect()(Field)

export default Field