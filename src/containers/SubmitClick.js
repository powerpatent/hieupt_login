import { connect } from 'react-redux'
import SubmitButton from '../components/SubmitButton'

const mapDispatchToProps = (state) =>{
    
    return {
        // do some thing
        handleClick: () => {
            console.log('_Log Submit: email is ', state.email, '|| password: ', state.password)
            // dispatch(actionLogin(state.email, state.password))
        }
    }
}

const SubmitClick = connect(
    mapDispatchToProps
)(SubmitButton)

export default SubmitClick