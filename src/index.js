import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import './index.css';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import {Provider} from 'react-redux'
import {createStore} from 'redux'

import reducerLogin from './reducers/reducers' 
import LoginForm from './components/LoginForm'

let store = createStore(reducerLogin)

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <LoginForm/>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);


