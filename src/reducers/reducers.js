import {CHANGE_EMAIL, CHANGE_PASSWORD} from '../actions/actionLogin'

function reducerLogin(state={}, action){
    switch (action.type){
        case CHANGE_EMAIL:
            return Object.assign({}, state, {
                email: action.email
            })
        
        case CHANGE_PASSWORD:
            return Object.assign({}, state, {
                password: action.password
            })
        
        default:
            return state
    }
}

export default reducerLogin